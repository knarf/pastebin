;;; pastebin.el --- paste stuff to http://pastebin.ca

;; Copyright (C) 2012-2013 Aurélien Aptel

;; Version: 1.0
;; Author: Aurélien Aptel <aurelien.aptel@gmail.com>
;; Keywords: comm

;; This file is not part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Paste any text to http://pastebin.ca with `pastebin-buffer' or
;; `pastebin-region'. Use `pastebin-send' in Lisp programs.

;;; Todo:

;; Add a generic interface for any pastebin-like site out there.

;;; Code:

(eval-when-compile (require 'cl))
(require 'url)

(defconst pastebin-api "d0vriwyPDknjjkvsgjkB1QuVUyYgMz74")
(defconst pastebin-root "http://pastebin.ca")
(defconst pastebin-url (concat pastebin-root "/quiet-paste.php?api=" pastebin-api))

(defconst pastebin-expire-list
  '("Never" "5 minutes" "10 minutes" "15 minutes" "30 minutes" "45 minutes"
    "1 hour" "2 hours" "4 hours" "8 hours" "12 hours" "1 day" "2 days"
    "3 days" "1 week" "2 weeks" "3 weeks" "1 month" "2 months" "3 months"
    "4 months" "5 months" "6 months" "1 year"))

;; ORDER MATTERS! the type sent is the position in the list (starting at 1)
(defconst pastebin-type-list
  '("Raw" "Asterisk Configuration" "C Source" "C++ Source" "PHP Source"
    "Perl Source" "Java Source" "Visual Basic Source" "C# Source"
    "Ruby Source" "Python Source" "Pascal Source" "mIRC Script"
    "PL I Source" "XML Document" "SQL Statement" "Scheme Source"
    "Action Script" "Ada Source" "Apache Configuration" "Assembly (NASM)"
    "ASP" "BASH Script" "CSS" "Delphi Source" "HTML 4.Strict" "JavaScript"
    "LISP Source" "Lua Source" "Microprocessor ASM" "Objective C"
    "Visual Basic .NET"))

(defvar pastebin-default-expire "1 day")
(defvar pastebin-default-type "Raw")

;; from http://emacswiki.org/emacs/UrlPackage
(defun pastebin-url-post (url args)
  "Send ARGS to URL as a POST request."
  (let ((url-request-method "POST")
        (url-request-extra-headers
         '(("Content-Type" . "application/x-www-form-urlencoded")))
        (url-request-data
         (mapconcat (lambda (arg)
                      (concat (url-hexify-string (car arg))
                              "="
                              (url-hexify-string (cdr arg))))
                    args
                    "&")))
    (url-retrieve-synchronously url)))

(defun pastebin--get-keys (buf)
  (let ((key nil)
        (keysig nil)
        (keyrx (rx "\"postkey\" value=\"" (group (+ (not (any "\""))))))
        (keysigrx (rx "\"postkeysig\" value=\"" (group (+ (not (any "\"")))))))

    (with-current-buffer buf
      (goto-char (point-min))
      (when (re-search-forward keyrx nil t)
        (setq key (match-string 1)))

      (goto-char (point-min))
      (when (re-search-forward keysigrx nil t)
        (setq keysig (match-string 1))))

    (kill-buffer buf)

    `(("postkey" . ,key) ("postkeysig" . ,keysig))))

(defun pastebin--get-url (buf)
  (with-current-buffer buf
      (goto-char (point-min))
      (when (re-search-forward (rx "SUCCESS:" (group (* anything))) nil t)
        (prog1
            (concat pastebin-root "/" (match-string 1))
          (kill-buffer buf)))))

(defun pastebin--get-type (type)
  (let ((pos (position type pastebin-type-list :test 'string=)))
    (when pos
      (number-to-string (1+ pos)))))

(defun pastebin-send (content &optional title desc type exp prompt)
  "Return paste url of CONTENT with TITLE, DESC, TYPE and EXP on
pastebin.ca. If PROMPT is non-nil, ask use the minibuffer to get
the missing arguments."
  (interactive)
  (if prompt
      (setq title (or title
                      (read-string
                       (format "Paste title (default %s): " (buffer-name))
                       nil nil (buffer-name)))
            desc (or desc
                     (read-string "Paste description: "))
            type (or type
                     (pastebin--get-type
                      (completing-read
                       (format "Paste type (default %s): " pastebin-default-type)
                       pastebin-type-list nil t nil nil pastebin-default-type)))
            exp (or exp
                    (completing-read
                     (format "Paste expiration (default %s): " pastebin-default-expire)
                     pastebin-expire-list nil t nil nil pastebin-default-expire)))
    ;; else
    (setq
     title (or title "")
     desc (or desc "")
     type (or type (pastebin--get-type pastebin-default-type))
     exp (or exp pastebin-default-expire)))

  (when (let ((ntype (string-to-number type)))
          (or (< ntype 1) (> ntype (length pastebin-type-list))))
    (error "Not a valid type"))

  (when (not (member exp pastebin-expire-list))
    (error "Not a valid expiration time"))

  (when (= 0 (length content))
    (error "No content to paste"))

  (let ((form
         `(("content"     . ,content)
           ("name"        . ,title)
           ("description" . ,desc)
           ("type"        . ,type)
           ("expiry"      . ,exp)
           ("s"           . "Submit Post")
           ,@(pastebin--get-keys
              (url-retrieve-synchronously pastebin-url)))))

    (pastebin--get-url (pastebin-url-post pastebin-url form))))

(defun pastebin-buffer ()
  "Paste current buffer and set kill buffer to the url.
Return the paste url."
  (interactive)
  (let ((url (pastebin-send (buffer-string) nil nil nil nil 'prompt)))
    (message "Paste at %s (url copied in kill-ring)" url)
    (kill-new url)))

(defun pastebin-region (start end)
  "Paste region and set kill buffer to the url.
Return the paste url."
  (interactive "r")
  (let ((url (pastebin-send (buffer-substring start end) nil nil nil nil 'prompt)))
    (message "Paste at %s (url copied in kill-ring)" url)
    (kill-new url)))

(provide 'pastebin)

;;; pastebin.el ends here
